import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os
#os.chdir("/home/pi/lavalrotor_home_experiment")
from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting


"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_waschemachine.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
setup_dictionary = json.dumps(setup_json_dict, indent=2, default=str)
print(setup_dictionary)
print()
print("Sensor settings dictionary")
sensor_settings_dictionary = json.dumps(sensor_settings_dict, indent=2, default=str) #giving variable to the dictionary
print(sensor_settings_dictionary)
sensor_settings_dictionary = json.loads(sensor_settings_dictionary)
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
uuid_acceleration_sensor = sensor_settings_dictionary['ID']
# Outer dictionary to store acceleration sensor data
acceleration_data_dict = {}

# Inner dictionary structure for each acceleration sensor
inner_dict_template = {
    'acceleration_x': [],
    'acceleration_y': [],
    'acceleration_z': [],
    'timestamp': []
}

# Create entries in the outer dictionary using the UUID as the key
acceleration_data_dict[uuid_acceleration_sensor] = inner_dict_template.copy()

print(acceleration_data_dict)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe

i2c = board.I2C()
accelerometer = adafruit_adxl34x.ADXL345(i2c)
start_time = time.time() #start time at 0s
accelerometer_data =[] #list of measured values
timestamp_data = []
while True and time.time()-start_time < measure_duration_in_s: #loop will stop adter 20s
    acceleration = accelerometer.acceleration
    timestamp = time.time()-start_time
    timestamp_data.append(timestamp)
    print ("%f %f %f " %acceleration)
    time.sleep(0.001) # pause of 1ms after every measurement
    
    accelerometer_data.append(acceleration)
print(timestamp_data)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5

#saving data in dictionary
acceleration_data_dict[str(uuid_acceleration_sensor)]['acceleration_x'] = [point[0] for point in accelerometer_data]
acceleration_data_dict[str(uuid_acceleration_sensor)]['acceleration_y'] = [point[1] for point in accelerometer_data]
acceleration_data_dict[str(uuid_acceleration_sensor)]['acceleration_z'] = [point[2] for point in accelerometer_data]
acceleration_data_dict[str(uuid_acceleration_sensor)]['timestamp'] = timestamp_data

#writing dictionary in hdf file
with h5py.File(path_h5_file,"a")as f:
        rawdata_group = f.create_group("/Rawdata")
        for key, values in acceleration_data_dict.items(): #creating a hdf file with groups and subgroups of dictionary
            subgroup = rawdata_group.create_group(key)
            for subkey, subvalue in values.items():
               dataset = subgroup.create_dataset(subkey, data=subvalue)
               
               #adding attribute unit to dataset
               if subkey.startswith("acceleration"):
                   dataset.attrs["unit"] = "mm/s^2"
               elif subkey == "timestamp":
                   dataset.attrs["unit"] = "s"
        
        
       
f.close()
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
