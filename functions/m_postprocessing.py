"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    abs_values = np.sqrt(x**2 + y**2 + z**2)
    return abs_values

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
  # Create a linearly spaced array of interpolation points
    interpolation_points = np.linspace(min(time), max(time), len(time) * 2)

    # Use numpy's interp function for linear interpolation
    interpolated_values = np.interp(interpolation_points, time, data)

    return interpolation_points, interpolated_values


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    fft_result = np.fft.fft(x)
     # Calculate the time interval
    dt = time[1] - time[0]
    # Calculate the corresponding frequencies
    frequencies = np.fft.fftfreq(len(x), dt)
    
    # Take only the positive frequencies and corresponding amplitudes
    positive_frequencies = frequencies[:len(frequencies)//2]
    positive_fft_result = np.abs(fft_result[:len(fft_result)//2])
    
    return positive_fft_result, positive_frequencies